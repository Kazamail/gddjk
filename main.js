import Vue from 'vue'
import App from './App'
import myHeader from './components/myHeader.vue'
import myFooter from './components/myFooter.vue'
import diet from './pages/index/healthy/diet.vue'
import dietDetails from './pages/index/healthy/dietDetails.vue'
import bodyTest from './pages/index/healthy/bodyTest.vue'
import bodyTestDetails from './pages/index/healthy/bodyTestDetails.vue'
import reserve from './pages/index/healthy/reserve.vue'
Vue.config.productionTip = false;
Vue.component('MyHeader',myHeader);
Vue.component('MyFooter',myFooter);
Vue.component('diet',diet);
Vue.component('dietDetails',dietDetails);
Vue.component('bodyTest',bodyTest);
Vue.component('bodyTestDetails',bodyTestDetails);
Vue.component('reserve',reserve);
App.mpType = 'app' ;

const app = new Vue({
    ...App
})
app.$mount()
